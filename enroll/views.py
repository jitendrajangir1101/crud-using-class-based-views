from django.shortcuts import render , HttpResponseRedirect
from .forms import RegistrationForm
from .models import Registration
from django.views.generic.base import TemplateView ,RedirectView
from django.views import View
class UserAddShowView(TemplateView):
    template_name='show.html'
    def get_context_data(self,*args , **kwargs):
        context=super().get_context_data(**kwargs)
        fm=RegistrationForm()
        stud=Registration.objects.all()
        context={'st':stud , 'form':fm}
        return context
    def post(self,request):
        fm=RegistrationForm(request.POST)
        if fm.is_valid():
            name=fm.cleaned_data['name']
            email=fm.cleaned_data['email']
            mobile=fm.cleaned_data['mobile']
            password=fm.cleaned_data['password']
            reg = Registration(name=name , email=email,mobile=mobile , password=password)
            reg.save()
            return HttpResponseRedirect('/')



#def home(request):
 #   if request.method == 'POST':
  #      fm=RegistrationForm(request.POST)
   #     if fm.is_valid():
    #        name=fm.cleaned_data['name']
     #       email=fm.cleaned_data['email']
      #      mobile=fm.cleaned_data['mobile']
       #     password=fm.cleaned_data['password']
        #    reg = Registration(name=name , email=email,mobile=mobile , password=password)
         #   reg.save()
          #  fm=RegistrationForm()
   # else:
    #    fm=RegistrationForm()
    #stud=Registration.objects.all()
    #return render(request, 'show.html',{'form':fm ,'st':stud})
class Delete_Data(RedirectView):
    url='/'
    def get_redirect_url(self,*args ,**kwargs):
        del_id=kwargs['id']
        Registration.objects.get(pk=del_id).delete()
        return super().get_redirect_url(*args,**kwargs)

        
#def delete_data(request , id):
 #   if request.method=='POST':
  #      pi = Registration.objects.get(pk=id)
   #     pi.delete()
    #    return HttpResponseRedirect('/')
class Update_Data(View):
    def get(self,request,id):
        pi=Registration.objects.get(pk=id)
        fm=RegistrationForm(instance=pi)
        return render(request , 'update.html' ,{'form':fm})
    def post(self,request,id):
        pi=Registration.objects.get(pk=id)
        fm=RegistrationForm(request.POST ,instance=pi)
        if fm.is_valid():
            fm.save()
            return HttpResponseRedirect('/')


#def update_data(request , id):
 #   if request.method == 'POST':
  #      pi=Registration.objects.get(pk=id)
   #     fm=RegistrationForm(request.POST ,instance=pi)
    #    if fm.is_valid():
     #       fm.save()
      #      return HttpResponseRedirect('/')
            
    #else:
     #   pi=Registration.objects.get(pk=id)
      #  fm=RegistrationForm(instance=pi)
    #return render(request , 'update.html' ,{'form':fm})






