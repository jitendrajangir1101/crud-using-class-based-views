from django.contrib import admin
from .models import Registration

@admin.register(Registration)
class RegisterAdmin(admin.ModelAdmin):
    list_display=('id','name','email','mobile','password')


# Register your models here.
