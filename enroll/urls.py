from django.urls import path,include
from . import views

urlpatterns = [
   
    path('' , views.UserAddShowView.as_view(), name='show'),
    #path('delete/<int:id>/' , views.delete_data , name='delete_data'),
    path('delete/<int:id>/' ,views.Delete_Data.as_view(),name='delete_data'),
    #path('<int:id>/' , views.update_data , name='update_data'),
    path('<int:id>/' ,views.Update_Data.as_view(),name='update_data'),
    
    
]
